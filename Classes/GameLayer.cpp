#include "GameLayer.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include <time.h>
#include <random>

USING_NS_CC;
using namespace ui;
using namespace cocostudio::timeline;

GameLayer::GameLayer()
	:isMoved(false),
	score(0),
	highScore(0),
	touchDown(0, 0),
	scoreLabel(nullptr),
	highLabel(nullptr),
	winLabel(nullptr),
	lostLabel(nullptr),
	gameOver(nullptr)
{
}

Scene* GameLayer::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = GameLayer::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool GameLayer::init()
{
    if ( !Layer::init() )
    {
        return false;
    }

    //加载资源到缓存
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("cell.plist", "cell.png");

    auto rootNode = CSLoader::createNode("MainScene.csb");
    addChild(rootNode);

	//游戏结束场景
	auto layer = (Layer*)CSLoader::createNode("Layer.csb");
	addChild(layer, 10);
	gameOver = (Layout*)layer->getChildByTag(16);
	gameOver->setVisible(false);

	winLabel = (Text*)gameOver->getChildByTag(19);
	lostLabel = (Text*)gameOver->getChildByTag(17);


	scoreLabel = (Text*)rootNode->getChildByTag(20);
	highLabel = (Text*)rootNode->getChildByTag(21);

	//添加按钮回调
	auto btnRestart = (Button*)rootNode->getChildByTag(10);
	auto btnExit = (Button*)rootNode->getChildByTag(8);
	auto btnContinue = (Button*)gameOver->getChildByTag(18);
	
	btnRestart->addTouchEventListener([&] (Ref* pSender, Widget::TouchEventType type){
		if (type == Widget::TouchEventType::ENDED)
		{
			destory();
			initGame();
		}
	});

	btnExit->addTouchEventListener([](Ref* pSender, Widget::TouchEventType type) {
		if (type == Widget::TouchEventType::ENDED)
		{
			Director::getInstance()->end();
		}
		
	});
	
	btnContinue->addTouchEventListener([&](Ref* pSender, Widget::TouchEventType type) {
		if (type == Widget::TouchEventType::ENDED)
		{
			destory();
			initGame();

			if (gameOver->isVisible())
			{
				//gameOver->setEnabled(false);
				gameOver->setVisible(false);
			}
			
		}
	});
	

	//添加监听器
	auto listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);
	listener->onTouchBegan = CC_CALLBACK_2(GameLayer::onTouchBegan, this);
	listener->onTouchMoved = CC_CALLBACK_2(GameLayer::onTouchMoved, this);
	listener->onTouchEnded = CC_CALLBACK_2(GameLayer::onTouchEnded, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);


	initGame();

    return true;
}

void GameLayer::initGame()
{
	score = 0;
	isWin = false;

	winLabel->setVisible(false);
	lostLabel->setVisible(false);

	//显示分数
	scoreLabel->setText(StringUtils::format("%d", score));

	//加载最高分
	highScore = UserDefault::getInstance()->getIntegerForKey("1", 0);
	highLabel->setText(StringUtils::format("%d", highScore));

	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			auto cell = Cell::create();
			cell->setPosition(RC_CONVERT_TO_Y(j), RC_CONVERT_TO_X(i));
			addChild(cell, 1);

			cellMap[i][j] = cell;
		}
	}

	//c++11的随机数产生方式
	std::default_random_engine e(time(NULL));
	std::uniform_int_distribution<unsigned> u(0, 3);
	int row1 = u(e);
	int col1 = u(e);
	int row2 = u(e);
	int col2 = u(e);
	//这个循环是保证两个砖块的坐标不会重复
	do {
		row2 = u(e);
		col2 = u(e);
	} while (row1 == row2&&col1 == col2);

	//添加第一个砖块
	auto cell1 = cellMap[row1][col1];

	int isFour = e() % 10;
	if (isFour == 0) {
		cell1->setLevel(2);
		cell1->setVisible(true);
	}
	else {
		cell1->setLevel(1);
		cell1->setVisible(true);
	}

	appearAnimation(cell1);

	//添加第二个砖块
	auto cell2 = cellMap[row2][col2];
	isFour = e() % 10;
	if (isFour == 0) {
		cell2->setLevel(2);
	}
	else {
		cell2->setLevel(1);
	}

	appearAnimation(cell2);
}

void GameLayer::destory()
{
	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			cellMap[i][j]->removeFromParentAndCleanup(true);
		}
	}
}

void GameLayer::addCell()
{
	//c++11的随机数产生方式
	std::default_random_engine e(time(NULL));
	std::uniform_int_distribution<unsigned> u(0, 3);
	int row = u(e);
	int col = u(e);

	//这个循环是保证两个砖块的坐标不会重复
	do {
		row = u(e);
		col = u(e);
	} while (cellMap[row][col]->getLevel() != 0);

	//添加一个砖块
	auto cell = cellMap[row][col];

	//出现动画
	appearAnimation(cell);  

	int isFour = e() % 10;
	if (isFour == 0) {
		cell->setLevel(2);
		cell->setVisible(true);
	}
	else {
		cell->setLevel(1);
		cell->setVisible(true);
	}
}

bool GameLayer::combineRow()
{
	bool isCombine = false;

	//合并相同项
	for (int row = 0; row < 4; ++row)
	{
		for (int col = 0; col < 4; ++col)
		{
			auto currentCell = cellMap[row][col];

			if (currentCell->getLevel() >= 11)
			{
				isWin = true;
			}

			if (currentCell->getLevel() != 0)
			{
				int nextRowIndex = row + 1;

				while (nextRowIndex < 4)
				{
					auto nextCell = cellMap[nextRowIndex][col];

					if (nextCell->getLevel() != 0)
					{
						if (currentCell->getLevel() == nextCell->getLevel())
						{
							currentCell->setLevel(currentCell->getLevel() + 1);

							nextCell->setLevel(0);
							nextCell->setVisible(false);

							isCombine = true;

							//计算得分
							score += currentCell->getNum();
						}
						break;
					}
					++nextRowIndex;
				}
			}

		}
	}

	return isCombine;
}

bool GameLayer::combineCol()
{
	bool isCombine = false;

	//合并相同项
	for (int col = 0; col < 4; ++col)
	{
		for (int row = 0; row < 4; ++row)
		{
			auto currentCell = cellMap[row][col];
			
			if (currentCell->getLevel() >= 11)
			{
				isWin = true;
			}

			if (currentCell->getLevel() != 0)
			{
				int nextColIndex = col + 1;

				while (nextColIndex < 4)
				{
					auto nextCell = cellMap[row][nextColIndex];

					if (nextCell->getLevel() != 0)
					{
						if (currentCell->getLevel() == nextCell->getLevel())
						{
							currentCell->setLevel(currentCell->getLevel() + 1);

							nextCell->setLevel(0);

							isCombine = true;

							//计算得分
							score += currentCell->getNum();
						}
						break;
					}
					++nextColIndex;
				}
			}

		}
	}

	return isCombine;
}

bool GameLayer::isGameOver()
{
	for (int row = 0; row < 4; ++row)
	{
		for (int col = 0; col < 4; ++col)
		{
			auto currentCell = cellMap[row][col];

			if (currentCell->getLevel() == 0)
			{
				return false;
			}

			//上
			int nextIndex = row + 1;
			if (nextIndex < 4 && currentCell->getLevel() == cellMap[nextIndex][col]->getLevel())
			{
				return false;
			}

			//下
			nextIndex = row - 1;
			if (nextIndex >= 0 && currentCell->getLevel() == cellMap[nextIndex][col]->getLevel())
			{
				return false;
			}

			//左
			nextIndex = col - 1;
			if (nextIndex >= 0 && currentCell->getLevel() == cellMap[row][nextIndex]->getLevel())
			{
				return false;
			}

			//右
			nextIndex = col + 1;
			if (nextIndex < 4 && currentCell->getLevel() == cellMap[row][nextIndex]->getLevel())
			{
				return false;
			}
		}
	}

	return true;
}

void GameLayer::over()
{
	gameOver->setVisible(true);

	//胜利
	if (isWin)
	{
		winLabel->setVisible(true);
	}
	else
	{
		//失败
		lostLabel->setVisible(true);
	}
}

bool GameLayer::moveToUp()
{
	bool isMove = false;

	isMove = combineRow();

	//向上移动

	for (int row = 3; row >= 0; --row)
	{
		for (int col = 3; col >= 0; --col)
		{
			auto currentCell = cellMap[row][col];

			if (currentCell->getLevel() == 0)
			{
				int nextRowIndex = row - 1;

				while (nextRowIndex >= 0)
				{
					if (cellMap[nextRowIndex][col]->getLevel() != 0)
					{
						currentCell->setLevel(cellMap[nextRowIndex][col]->getLevel());

						cellMap[nextRowIndex][col]->setLevel(0);
						
						//标记是否移动过
						isMove = true;

						break;
					}
					--nextRowIndex;
				}
			}
		}

	}


	return isMove;
}

bool GameLayer::moveToDown()
{
	bool isMove = false;

	isMove = combineRow();

	//向下移动

	for (int row = 0; row < 4; ++row)
	{
		for (int col = 0; col < 4; ++col)
		{
			auto currentCell = cellMap[row][col];

			if (currentCell->getLevel() == 0)
			{
				int nextRowIndex = row + 1;

				while (nextRowIndex < 4)
				{
					if (cellMap[nextRowIndex][col]->getLevel() != 0)
					{
						currentCell->setLevel(cellMap[nextRowIndex][col]->getLevel());

						cellMap[nextRowIndex][col]->setLevel(0);
						

						//标记是否移动过
						isMove = true;

						break;
					}
					++nextRowIndex;
				}
			}
		}

	}
	

	return isMove;
}

bool GameLayer::moveToLeft()
{
	bool isMove = false;

	combineCol();

	//向左移动
	for (int col = 0; col < 4; ++col)
	{
		for (int row = 0; row <4; ++row)
		{
			auto currentCell = cellMap[row][col];

			if (currentCell->getLevel() == 0)
			{
				int nextColIndex = col + 1;

				while (nextColIndex < 4)
				{
					if (cellMap[row][nextColIndex]->getLevel() != 0)
					{

						currentCell->setLevel(cellMap[row][nextColIndex]->getLevel());

						cellMap[row][nextColIndex]->setLevel(0);

						//标记是否移动过
						isMove = true;

						break;
					}

					++nextColIndex;
				}
			}
		}

	}


	return isMove;
}

bool GameLayer::moveToRight()
{
	bool isMove = false;

	combineCol();

	//向右移动

	for (int col = 3; col >= 0; --col)
	{
		for (int row = 3; row >= 0; --row)
		{
			auto currentCell = cellMap[row][col];

			if (currentCell->getLevel() == 0)
			{
				int nextColIndex = col - 1;

				while (nextColIndex >= 0)
				{
					if (cellMap[row][nextColIndex]->getLevel() != 0)
					{

						currentCell->setLevel(cellMap[row][nextColIndex]->getLevel());

						cellMap[row][nextColIndex]->setLevel(0);

						//标记是否移动过
						isMove = true;

						break;
					}

					--nextColIndex;
				}
			}
		}

	}


	return isMove;
}

void GameLayer::appearAnimation(Cell* cell)
{
	cell->setScale(0);
	auto scale = ScaleTo::create(0.25f, 1.0);
	cell->runAction(scale);
}

bool GameLayer::onTouchBegan(Touch *touch, Event *unused_event) {
	this->touchDown = touch->getLocationInView();
	this->touchDown = Director::getInstance()->convertToGL(this->touchDown);
	return true;
}

void GameLayer::onTouchMoved(Touch *touch, Event *unused_event) {

}

void GameLayer::onTouchEnded(Touch *touch, Event *unused_event) {
	
	//如果在游戏矩形外滑动则忽略
	if(!Rect(30, 120, 580, 580).containsPoint(touchDown))
	{
		return;
	}

	bool hasMoved = false;

	Point touchUp = touch->getLocationInView();
	touchUp = Director::getInstance()->convertToGL(touchUp);

	if (touchUp.getDistance(touchDown) > 50) {
		//判断上下还是左右
		if (abs(touchUp.x - touchDown.x) > abs(touchUp.y - touchDown.y)) {
			//左右滑动
			if (touchUp.x - touchDown.x > 0) {
				//向右
				hasMoved = moveToRight();
			}
			else {
				//向左
				hasMoved = moveToLeft();
			}
		}
		else {
			//上下滑动
			if (touchUp.y - touchDown.y > 0) {
				//向上
				hasMoved = moveToUp();
			}
			else {
				//向下
				hasMoved = moveToDown();
			}
		}

	}

	if (hasMoved)
	{
		addCell();
	}

	//更新分数
	scoreLabel->setText(StringUtils::format("%d", score));

	if (isWin || isGameOver())
	{
		if (highScore < score)
		{
			UserDefault::getInstance()->setIntegerForKey("1", score);
		}

		//启动结束场景
		over();
	}
}