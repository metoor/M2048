#ifndef _Cell_H_
#define _Cell_H_
#include "cocos2d.h"

USING_NS_CC;

class Cell:public Sprite{
private:
	int level;
	Label *label;
	static const int nums[16];

public:
	Cell();
	virtual bool init();
	CREATE_FUNC(Cell);
	void setLevel(int l);
	int getLevel();
	int getNum();
	void moveTo(cocos2d::Point p);
};
#endif