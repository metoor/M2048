#ifndef __GAMELAYER_H_
#define __GAMELAYER_H_

#define RC_CONVERT_TO_Y(rc) (rc * 140.0 + 110)
#define RC_CONVERT_TO_X(rc) (rc * 140.0 + 200)

#include "cocos2d.h"
#include "Cell.h"
#include "ui/UIText.h"
#include "ui/UILayout.h"

class GameLayer : public cocos2d::Layer
{
public:
	GameLayer();
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // implement the "static create()" method manually
    CREATE_FUNC(GameLayer);

	void initGame();

	void destory();

	//是否合并纵向相同快
	bool combineRow();
	//是否合并横向相同快
	bool combineCol();
	//游戏是否结束
	bool isGameOver();

	void over();

	//添加一个方块
	void addCell();

	//向上移动
	bool moveToUp();
	//向下移动
	bool moveToDown();
	//向左移动
	bool moveToLeft();
	//向右移动
	bool moveToRight();

	//出现动画
	void appearAnimation(Cell* cell);

	virtual bool onTouchBegan(Touch *touch, Event *unused_event);
	virtual void onTouchMoved(Touch *touch, Event *unused_event);
	virtual void onTouchEnded(Touch *touch, Event *unused_event);

private:
	Cell* cellMap[4][4];
	int	score;
	int highScore;
	Point touchDown;
	bool isMoved;
	bool isWin;
	cocos2d::ui::Text* scoreLabel;
	cocos2d::ui::Text* highLabel;
	cocos2d::ui::Text* winLabel;
	cocos2d::ui::Text* lostLabel;
	cocos2d::ui::Layout* gameOver;
};

#endif // __GameLayer_SCENE_H__
