#include "Cell.h"

const int Cell::nums[16]={0,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768};
Cell::Cell(){
	level=0;
	this->setVisible(false);
}

bool Cell::init(){
	bool bRet=false;
	do{
		CC_BREAK_IF(!Node::init());

		auto cache=SpriteFrameCache::getInstance();
		this->initWithSpriteFrameName("num_0.png");

		this->label=Label::create(String::createWithFormat("%d",Cell::nums[level])->getCString(),"Arial",40);
		this->label->setPosition(Point(getContentSize().width / 2, getContentSize().height / 2));
		this->addChild(label,1);

		bRet=true;
	}while(0);

	return bRet;
}

void Cell::setLevel(int l){
	auto cache=SpriteFrameCache::getInstance();
	this->level=l;
	this->setDisplayFrame(cache->spriteFrameByName(String::createWithFormat("num_%d.png",Cell::nums[level])->getCString()));
	this->label->setString(String::createWithFormat("%d",Cell::nums[level])->getCString());
	
	if(l == 0)
	{
		this->setVisible(false);
	}
	else if (!this->isVisible())
	{
		this->setVisible(true);
	}
	
}

int Cell::getLevel()
{
	return level;
}

int Cell::getNum()
{
	return nums[level];
}

void Cell::moveTo(Point p)
{
	auto move = MoveTo::create(0.25f, p);
	this->runAction(move);
}
